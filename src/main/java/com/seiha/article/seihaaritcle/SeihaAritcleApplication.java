package com.seiha.article.seihaaritcle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeihaAritcleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeihaAritcleApplication.class, args);
    }
}
