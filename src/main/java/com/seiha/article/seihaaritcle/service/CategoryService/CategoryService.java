package com.seiha.article.seihaaritcle.service.CategoryService;

import com.seiha.article.seihaaritcle.model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
    Category findOne(int id);
}
