package com.seiha.article.seihaaritcle.service;

import com.seiha.article.seihaaritcle.model.Article;
import com.seiha.article.seihaaritcle.model.ArticleFilter;
import com.seiha.article.seihaaritcle.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImplement implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;
    @Override
    public void insert(Article article) {
        articleRepository.insert(article);
    }

    @Override
    public Article findOne(int id) {
        return articleRepository.findOne(id);
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id);
    }
    @Override
    public void update(Article article) {
        articleRepository.update(article);
    }

    @Override
    public List<Article> findAllFilter(ArticleFilter filter) {
        return articleRepository.findAllFilter(filter);
    }
}
