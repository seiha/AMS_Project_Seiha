package com.seiha.article.seihaaritcle.service.CategoryService;

import com.seiha.article.seihaaritcle.model.Category;
import com.seiha.article.seihaaritcle.repository.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImplement implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findOne(int id) {
        return categoryRepository.findOne(id);
    }
}
