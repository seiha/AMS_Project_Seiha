package com.seiha.article.seihaaritcle.controller;

import com.seiha.article.seihaaritcle.model.Article;
import com.seiha.article.seihaaritcle.model.ArticleFilter;
import com.seiha.article.seihaaritcle.service.ArticleService;
import com.seiha.article.seihaaritcle.service.CategoryService.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
@PropertySource("classpath:ams.properties")
public class ArticleController {


    @Autowired
    private ArticleService articleService;
    @Autowired
    private CategoryService categoryService;


    @Value("${file.server.path}")
    private String serverPath;

    @GetMapping(value = {"/home", "/index", "/"})
    public String redirectToHome(){
        return "redirect:/article";
    }

    @GetMapping("/article")
    public String article(ArticleFilter filter ,ModelMap model){
        System.out.println("Filter:" + filter);
        model.addAttribute("filter", filter);
        model.addAttribute("articles", articleService.findAllFilter(filter));
        return "article";
    }

    @GetMapping("/article/{id}")
    public String article(@PathVariable("id") int id, ModelMap model){
        model.addAttribute("article", articleService.findOne(id));
        return "article_detail";
    }

    @GetMapping("/add")
    public String addArticle( ModelMap m){
        m.addAttribute("formAdd", true);
        m.addAttribute("categories", categoryService.findAll());
        m.addAttribute("article",new Article());
        return "add";
    }
    @PostMapping("/add")
    public String saveArticle(@RequestParam("image") MultipartFile file, @Valid @ModelAttribute Article article, BindingResult result, Model m){
        System.out.println(article);
        if (result.hasErrors() || file.isEmpty()){
            m.addAttribute("formAdd", true);
            return "add";
        }
        else{
            String fileName = file.getOriginalFilename();
            String extension = fileName.substring(fileName.lastIndexOf('.')+1);
            fileName = UUID.randomUUID() + "."+ extension;
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath, fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            article.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            article.setCategory(categoryService.findOne(article.getCategory().getId()));
            article.setThumbnail("/image/"+fileName);
            articleService.insert(article);
            return "redirect:/article";
        }
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") int id, Model model){
        model.addAttribute("formAdd", false);
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("article", articleService.findOne(id));
        return "add";
    }
    @PostMapping("/update")
    public String saveChanged(@Valid @ModelAttribute Article article,
                              BindingResult bindingResult,
                              Model model,
                              @RequestParam("image") MultipartFile file) {
//                    System.out.println(articleService.findOne(article.getId()).getCreatedDate());
        if (bindingResult.hasErrors()) {
            model.addAttribute("formAdd", false);
            System.out.println(article.getCategory());
            return "add";
        } else {
            String fileName = file.getOriginalFilename();
            String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
            fileName = UUID.randomUUID() + "." + extension;
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath, fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
//            System.out.println(fileName);
            article.setThumbnail("/image/" + fileName);
            article.setCategory(categoryService.findOne(article.getCategory().getId()));
//            System.out.println(articleService.findOne(article.getId()).getCreatedDate());
            article.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            System.out.println(article.getId());
            articleService.update(article);
            return "redirect:/article";
        }

    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id){
        articleService.delete(id);
        return "redirect:/article";
    }
}
